package com.devcamp.restapi.control;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.model.*;
import com.devcamp.restapi.service.ClassroomService;
import com.devcamp.restapi.service.SchoolService;

@RestController
@CrossOrigin
public class Control {
    @Autowired 
    private SchoolService schoolService;
    @Autowired
    private ClassroomService classroomService;


    @GetMapping("/shools") // lấy ra toàn bộ danh sách trường
    public ArrayList<School> getAllSchool(){
        // SchoolService schoolService = new SchoolService();
        return schoolService.getAllSchool();
    }
    
    @GetMapping("/shool") // lấy ra School có id tương ứng
    public ArrayList<School> getAllSchool(@RequestParam(defaultValue = "0") int id ){
        // SchoolService schoolService = new SchoolService();        
        ArrayList<School> allSchool = schoolService.getAllSchool();
  
        ArrayList<School> resultSchool = new ArrayList<>();  
        for (int i = 0; i < allSchool.size(); i++){
            if (allSchool.get(i).getId() == id){
                resultSchool.add(allSchool.get(i));
            }
        }
        return resultSchool;

        // for (School school : allSchool) {
        //     if(school.getId() == id) {
        //         resultSchool = school;
        //     }
        //     else {
        //         resultSchool = allSchool;
        //     }
        // }
        // return resultSchool;
    }

    @GetMapping("/classes") // trả ra danh sách tất cả các lớp
    public ArrayList<Classroom> getAllClass(){
        // ClassroomService classroomService = new ClassroomService();
        return classroomService.getListAllCLass();
    }

    @GetMapping("/class-size")
    public ArrayList<Classroom> getClassBySize(@RequestParam(defaultValue = "0") int size){
        // ClassroomService classroomService = new ClassroomService();
        ArrayList<Classroom> allClass = classroomService.getListAllCLass();

        ArrayList <Classroom> resultClass = new ArrayList<>();
        for (int i = 0; i < allClass.size(); i++){
            if (allClass.get(i).getNoStudent() > size){
                resultClass.add(allClass.get(i));
            }
        }
        return resultClass;
    }

    @GetMapping("/school-size")
    public ArrayList<School> getSchoolBySize(@RequestParam(defaultValue = "0") int size){
        // SchoolService schoolService = new SchoolService();        
        ArrayList<School> allSchool = schoolService.getAllSchool();

        ArrayList <School> resultSchool = new ArrayList<>();
        for (int i = 0; i < allSchool.size(); i++){
            if (allSchool.get(i).getTotalStudent() > size){
                resultSchool.add(allSchool.get(i));
            }
        }
        return resultSchool;
    }

}
