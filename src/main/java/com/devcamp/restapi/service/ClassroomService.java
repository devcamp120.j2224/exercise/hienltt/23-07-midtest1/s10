package com.devcamp.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.restapi.model.Classroom;

@Service
public class ClassroomService {
    Classroom classroom1 = new Classroom(11, "Tự Nhiên", 38);
    Classroom classroom2 = new Classroom(12, "Xã Hội", 45);
    Classroom classroom3 = new Classroom(13, "Chuyên Toán", 32);
    Classroom classroom4 = new Classroom(14, "Chuyên Lý", 30);
    Classroom classroom5 = new Classroom(15, "Chuyên Hoá", 37);
    Classroom classroom6 = new Classroom(16, "Chuyên Anh", 41);
    Classroom classroom7 = new Classroom(17, "Năng Khiếu", 48);
    Classroom classroom8 = new Classroom(18, "Khoa Học", 33);
    Classroom classroom9 = new Classroom(19, "Tự động hoá", 39);

    public ArrayList<Classroom> getListCLassShoolI(){
        ArrayList<Classroom> listClassOfSchoolI = new ArrayList<>();
        listClassOfSchoolI.add(classroom1);
        listClassOfSchoolI.add(classroom2);
        listClassOfSchoolI.add(classroom3);
        return listClassOfSchoolI;
    }

    public ArrayList<Classroom> getListCLassShoolII(){
        ArrayList<Classroom> listClassOfSchoolII = new ArrayList<>();
        listClassOfSchoolII.add(classroom4);
        listClassOfSchoolII.add(classroom5);
        listClassOfSchoolII.add(classroom6);
        return listClassOfSchoolII;
    }

    public ArrayList<Classroom> getListCLassShoolIII(){
        ArrayList<Classroom> listClassOfSchoolIII = new ArrayList<>();
        listClassOfSchoolIII.add(classroom7);
        listClassOfSchoolIII.add(classroom8);
        listClassOfSchoolIII.add(classroom9);
        return listClassOfSchoolIII;
    }

    public ArrayList<Classroom> getListAllCLass(){
        ArrayList<Classroom> listAllClass = new ArrayList<>();
        listAllClass.add(classroom1);
        listAllClass.add(classroom2);
        listAllClass.add(classroom3);
        listAllClass.add(classroom4);
        listAllClass.add(classroom5);
        listAllClass.add(classroom6);
        listAllClass.add(classroom7);
        listAllClass.add(classroom8);
        listAllClass.add(classroom9);
        return listAllClass;
    }

}
