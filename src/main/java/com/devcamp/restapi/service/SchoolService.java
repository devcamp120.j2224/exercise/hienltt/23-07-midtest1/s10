package com.devcamp.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.restapi.model.School;

@Service
public class SchoolService {
    
    public ArrayList<School> getAllSchool(){

        School school1 = new School(99, "SCHOOL1", "Gò Vấp", new ClassroomService().getListCLassShoolI());
        School school2 = new School(88, "SCHOOL2", "Tân Bình", new ClassroomService().getListCLassShoolII());
        School school3 = new School(77, "SCHOOL3", "Phú Nhuận", new ClassroomService().getListCLassShoolIII());

        ArrayList<School> listSchool = new ArrayList<>();
        listSchool.add(school1);
        listSchool.add(school2);
        listSchool.add(school3);

        return listSchool;
    }
}
